\d .util

// @kind function
// @param path  {symbol}    this is the path which needs to be tested for the file location
// @fileOverview This is a function that checks if a particular path is valid or not, typically used from within loadCSV function
checkFileExists:{[path]
    / Throws error if path doesn't exist
    if[0=count key path;
        '`$"File :",(string path)," does not exist"
     ];
 };

// @kind function
// @param delimiter {char}      this is the character which is the delimiter for the file to be read
// @param cfg       {table}     this is the schema config defined in schemas.q that is of the form <csvCol;fmt>
// @param hasHeader {boolean}   this tells if the file being read has a header or not
// @param path      {symbol}    this is the path which needs to be tested for the file location
// @fileOverview This is a function that checks if a particular path is valid or not, typically used from within loadCSV function
readFile:{[delimiter;cfg;hasHeader;path]
    
    /Check if the file exists
    checkFileExists path;

    /Actual read
    (cfg `fmt;$[hasHeader;(),delimiter;delimiter]) 0:path
 };

// @kind function
// @fileOverview This is a function that is used to read the task1Data csv and return a table
// @returns {table} task1Data is returned
getTask1Data:{
    t:readFile[",";.schemas.task1Data;1b;hsym `$.init.code,"/data/",.cfg.task1FileName];
    / Update `p# attribute as the data has already been sorted on time and is parted on symbols
    / It's not needed on small sized datasets, but assuming in production the data will be in millions
    update `p#fxSym from t
 };

// @kind function
// @fileOverview This is a function that is used to read the task2ClOrdsData csv and return a table
// @returns {table} task2ClOrdsData is returned
getTask2ClOrdsData:{
    readFile[",";.schemas.task2ClOrdsData;1b;hsym `$.init.code,"/data/",.cfg.task2ClOrdsFileName]
 };

// @kind function
// @fileOverview This is a function that is used to read the task2MktTradesData csv and return a table
// @returns {table} task2MktTradesData is returned
getTask2MktTradesData:{
    t:readFile[",";.schemas.task2MktTradesData;1b;hsym `$.init.code,"/data/",.cfg.task2MktDataFileName];
    / Update `p# attribute as the data has already been sorted on time and is parted on symbols
    / It's not needed on small sized datasets, but assuming in production the data will be in millions
    update `p#sym from t
 };

// @kind function   
// @fileOverview This is a function that returns FX trades from task1Data
getFXTrades:{[filter]
    `fxSym xgroup ?[`.[`task1Data];((),filter);0b;()]
 };

// @kind function   
// @fileOverview This is a function that returns Equities marketTrades from task2MktTradesData
getEquitiesTrades:{[filter]
    `sym xgroup ?[`.[`task2MktTradesData];((),filter);0b;()]
 };

\d .
