\d .testDataGen

// @kind function
// @fileOverview This is a function uses to generate dummy data for task1
// the data is primarily randomized FX_Rates for a bunch of FX pairs
generateTask1Data:{
    /How many rows of data
    n:.cfg.numOfMktDataRows;
    show "Generating random datasets for Task 1 of the count ", string n;

    /Generate (count FXPairs) vectors of n timestamps
    times:asc each flip (n;(count .cfg.task1FXPairs))#0D00:00:00.000000000+(n*count .cfg.task1FXPairs)?0D23:59:00.000000000;

    /Generate (count FXPairs) vectors of n sizes
    sizes:flip (n;(count .cfg.task1FXPairs))#100*(n*count .cfg.task1FXPairs)?1000j;
    
    /Generate (count FXPairs) vectors of n prices
    prices:{x+y?0.01}[;n] each value .cfg.task1EODFX;

    /Generate the final table
    data:`fxSym`time xcols ({[data;fx;time;price;size] data,update fxSym:fx from ([]time:time;price:price;size:size)}/) [([]fxSym:0#`;time:0#0Nn;price:0#0n;size:0#0N);.cfg.task1FXPairs;times;prices;sizes];

    /Dump the data into a CSV in the data directory
    (hsym `$.init.code,"/data/",.cfg.task1FileName) 0: csv 0:data;

    show "Task 1 dataset written to path - ", string `$.init.code,"/data/",.cfg.task1FileName;
 };

// @kind function
// @fileOverview This is a function uses to generate dummy data for task2
// there are 2 datasets generated in this function - 
//      a) clientOrders with <id;version;sym;time;side;limit;start;end> columns for a few RICs
//      b) marketTrades table with <sym;time;price;size> columns for a few RICs
generateTask2Data:{
    /How many different clientOrdersdata
    n_ords:.cfg.numOfOrders;
    show "Generating random clientOrders dataset for Task 2 using ",(string n_ords)," orders";

    /Total number of rows
    n_total:.cfg.numOfOrderAuditTrail;
    id:1+n_total?n_ords;

    /Pick a symbol from the list of RICs and a side and assign it to orders
    clOrds:`id xasc ([]id:id) lj `id xkey delete from (([]id:1+til n_ords) lj `id xkey ([]id:1+til count cnt_syms;sym:cnt_syms:n_ords?.cfg.task2RICs)) where null sym;
    clOrds:clOrds lj `id xkey ([] id:exec distinct id from clOrds;side:(first exec count distinct id from clOrds)?(`B`S));

    /Assign version numbers
    clOrds:`id`version xasc update version:(0+til count i) by id from clOrds;

    /Assign a limitPx based on the symbol - leave one order without limitPx to test all scenarios
    clOrds:clOrds lj `sym xkey -1_flip `sym`limit!((key .cfg.task2LimitPx);value .cfg.task2LimitPx);

    /Modify limitPx across some of the versions (50% probability)
    clOrds:update limit:?[0=n_total?(0;1);limit;limit+0.1*n_total?(-10+1+til 20)] from clOrds;

    /Add time column, start from 8am and then generate random time intervals for each version
    clOrds:update time:(08:00:00.000+asc (count i)?08:00:00.000) by id from clOrds;

    /Mark start and endTimes based on final events
    clOrds:update startTime:(first time), endTime:((last time)+(count i)?00:00:10.000) by id from clOrds;
    clOrds:update endTime:(last endTime) by id from clOrds;

    /Dump the data into a CSV in the data directory
    (hsym `$.init.code,"/data/",.cfg.task2ClOrdsFileName) 0: csv 0:clOrds;
    show "Task 2 clientOrders dataset written to path - ", string `$.init.code,"/data/",.cfg.task2ClOrdsFileName;

    show "Generating random marketTrades dataset for Task 2";
    /How many rows of data
    n:.cfg.numOfMktDataRows;

    /Generate (count RICs) vectors of n timestamps
    times:asc each flip (n;(count .cfg.task2RICs))#08:00:00.000+(n*count .cfg.task2RICs)?08:00:00.000;

    /Generate (count RICs) vectors of n sizes
    sizes:flip (n;(count .cfg.task2RICs))#10*(n*count .cfg.task2RICs)?1000j;
    
    /Generate (count RICs) vectors of n prices
    prices:{x+y?0.01*(-150+til 300)}[;n] each value .cfg.task2EODPx;

    /Generate the final table
    data:`sym`time xcols ({[data;sym;time;price;size] data,update sym:sym from ([]time:time;price:price;size:size)}/) [([]sym:0#`;time:0#0Nt;price:0#0n;size:0#0N);.cfg.task2RICs;times;prices;sizes];

    /Dump the data into a CSV in the data directory
    (hsym `$.init.code,"/data/",.cfg.task2MktDataFileName) 0: csv 0:data;
    show "Task 2 marketTrades dataset written to path - ", string `$.init.code,"/data/",.cfg.task2MktDataFileName;
 };