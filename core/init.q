\d .init

\c 250 250

// @kind function
// @fileOverview This function loads a particular library/Q file in the codebase
// @param codePath  {string}    this is the base path where the code is located
// @param libPath   {string}    this is the relative path of a file within the codebase
// @returns         {boolean}   signifying if the load of the particular library/Q file was successful or not
loadLibrary:{[codePath; libPath]
    if[type key hsym `$codePath, libPath;
        system "l ", codePath, libPath;
        :1b
    ];
    :0b
 };

// @kind function
// @fileOverview This function is called from loadCode and used to initialize all required .q files in the appropriate sequqence
// any new file needs to be added in there if required to be loaded by the working process
loadRepository:{
    .init.code:(getenv`codePath),"/";

    filesToLoad:(
        "/cfg/cfg.q";
        "/schemas/schemas.q";
        "/utils/testDataGenerator.q";
        "/utils/utils.q";
        "/mkt/mkt.q";
        "/api/api.q"
    );

    loadStatus:all loadLibrary[.init.code;] each filesToLoad;

    $[loadStatus;
        show "All code files loaded successfully";
        [
            show "Failure in loading a file via .init.q.... Exiting";
            exit 1
        ]
    ];
 };

// @kind function
// @fileOverview This is the base function that sets up the code path and loads up all of the workspace files one-by-one
loadCode:{
    / Parse the input string to the Q command line to retrieve the actual repository path
    path:first .Q.opt[.z.x][`codePath];

    / If path not specified, point it to current
    $[path~();
        [
            show "Path not specified. Overridding it to current directory...";
            path:"./"
        ];
        show "Path has been specified as - ", path
    ];

    / Set environment variable to reference in loading libraries
    setenv[`codePath;path];

    / Load all files in the repository
    loadRepository[]
 };

/ Actual call to initizlize the entire repo
loadCode[];

\d .

/ Initialize the two datasets
task1Data:.util.getTask1Data[];
task2ClOrdsData:.util.getTask2ClOrdsData[];
task2MktTradesData:.util.getTask2MktTradesData[];

show "All data files loaded successfully";
show "You can access the data in these tables - ","," sv string system("v");