\d .schemas

/Schema for Task1 dataset
task1Data:flip `csvCol`fmt!flip 
    ((`fxSym    ;"s");
     (`time     ;"n");
     (`price    ;"f");
     (`size     ;"j")
    );

/Schema for Task2 clientOrders dataset
task2ClOrdsData:flip `csvCol`fmt!flip 
    ((`id       ;"j");
     (`sym      ;"s");
     (`side     ;"s");
     (`version  ;"j");
     (`limit    ;"f");
     (`time     ;"t");
     (`startTime;"t");
     (`endTime  ;"t")
    );

/Schema for Task2 mktTrades dataset
task2MktTradesData:flip `csvCol`fmt!flip 
    ((`sym      ;"s");
     (`time     ;"t");
     (`price    ;"f");
     (`size     ;"j")
    );



