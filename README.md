# Kdb_Tasks

## Introduction
This Gitlab project has been created as part of the FICC IT - Kdb assignment
Q code has been provided for Tasks 1 & 2 (including test cases), while Task3 has been answered via task3Design.docx file under /api/

## Getting started
After checking out/cloning the project, the only thing to keep in mind is the <dirPath> where the code has been downloaded.
For instance, dirPath could be - "C:\Users\Downloads\kdb_tasks"
This dirPath is passed in as an optional argument when the Q process is launched. To start, run the below on command line - 
```
q c:/Users/sunny/Desktop/TestCode/kdb_tasks/core/init.q -codePath "C:\Users\sunny\Desktop\TestCode\kdb_tasks"
```

If -codePath is not specified, codePath will be set as the current working directory .
As the Q process starts, the init.q file will set .init.code as the base directory and load all relevant libraries one-by-one. You will see something like below post the Q process start - 
```
"Path has been specified as - C:\\Users\\sunny\\Desktop\\TestCode\\kdb_tasks"
"All code files loaded successfully"
"All data files loaded successfully"
"You can access the data in these tables - task1Data,task2ClOrdsData,task2MktTradesData"
q)
```

As noted above, the datasets for the 2 tasks are automatically loaded on startup and stored as - 
- task1Data         : this is the FXRates dataset created for task1
- task2ClOrdsData   : this is the client orders dataset created for task2
- task2MktTradesData: this is the Equities market trades dataset created for task2

## Code structure
The code structure can be elaborated as below - 

- kdb_tasks
    - api
        - api.q  ==> Contains the main API calls for the project
        - task3.docx ==> Contains a design document for Task3
    - cfg
        - cfg.q  ==> Contains configs that are used throughout the project
    - core
        - init.q ==> The startup file which is responsible for loading all the other code files
    - data
        - All testData sets are stored here by the testDataGenerator code
    - mkt
        - mkt.q ==> Underlying market data functions like VWAP/TWAP implementation are defined here
    - schemas
        - schemas.q ==> Schemas defined to read the dataset CSVs in-memory at startup
    - tests
        - k4unit.q ==> Copy of Simon's k4Unit library
        - KUTR.csv ==> Every time test cases are run, results are stored here
        - run_tests.q ==> Script to run test cases defined in testCases.csv
        - testCases.csv ==> All unit testcases defined in this file. For more info on the format of this file, refer here - https://code.kx.com/q/kb/unit-tests/#writing-descriptions
    - utils
        - testDataGenerator.q ==> Script used to generate datasets used by APIs for Tasks 1 & 2
        - utils.q ==> Any generic utils that may be used by other parts of the codebase

## API Calls
There are 3 major APIs defined in /api/api.q related to the 2 tasks - 
- .api.getTask1Vwap ==> this API will take in a single/list of FX pairs (symbols), startTime, endTime (in timespan) and return the interval Volume Weighted Average Price (VWAP) for each FX pair. Example run given below - 
```
q).api.getTask1Vwap[`GBPINR`EURUSD;0D09:00:00.000000000;0D09:00:02.000000000]
fxSym  startTime            endTime              vwap
---------------------------------------------------------
GBPINR 0D09:00:00.000000000 0D09:00:02.000000000 103.128
EURUSD 0D09:00:00.000000000 0D09:00:02.000000000 1.066647
```

- .api.getTask1Twap ==> this API will take in a single/list of FX pairs (symbols), startTime, endTime (in timespan) and return the interval Time Weighted Average Price (TWAP) for each FX pair. Example run given below - 
```
q).api.getTask1Twap[`GBPINR`EURUSD;0D09:00:00.000000000;0D09:00:02.000000000]
fxSym  startTime            endTime              twap
---------------------------------------------------------
GBPINR 0D09:00:00.000000000 0D09:00:02.000000000 103.1287
EURUSD 0D09:00:00.000000000 0D09:00:02.000000000 1.066024
```

- .api.getTask2Vwap ==> this API takes in a orderId (long) and returns Volume Weighted Average Price (VWAP) for the entire duration of the order taking into account the variation in limitPrice during the order lifetime. Example run given below - 
```
q).api.getTask2Vwap[2]
sym   side startTime    endTime      vwap
---------------------------------------------
ISF.L S    09:46:52.001 12:25:25.346 773.2411
```

## Dataset generation
On startup, the process loads the already created datasets mentioned above from /data/. However, the scripts used to generate these datasets have been included in /utils/testDataGenerator.q
If needed, we can tweak the params in /cfg/cfg.q and the datasets can be re-generated with different FXPairs or more row counts. On running the dataset generation functions, new randomized datasets are generated and dumped under /data/. again, replacing the old files. On subsequent restart of the process OR reloading of the init file, new datasets are loaded into in-memory variables
To create the datasets, simple run the below - 
```
.testDataGen.generateTask1Data[]
.testDataGen.generateTask2Data[]
```

## Test cases
I have used Simon Garland's k4unit.q to add unit test cases to this project. The /tests/k4unit.q is a copy-paste from Simon's original repo at - https://github.com/simongarland/k4unit. Test-cases have been defined in /tests/testCases.csv and can be run via the below call - 
```
q c:/Users/sunny/Desktop/TestCode/kdb_tasks/tests/run_tests.q -codePath "C:\Users\sunny\Desktop\TestCode\kdb_tasks"
KDB+ 4.0 2023.01.20 Copyright (C) 1993-2023 Kx Systems
w64/ 8(24)core 32538MB sunny sundew-zbook 192.168.1.114 EXPIRE 2024.07.13 sunnyshukla2008@gmail.com KOD #5015196

"Path has been specified as - C:\\Users\\sunny\\Desktop\\TestCode\\kdb_tasks"
"All code files loaded successfully"
"All data files loaded successfully"
"You can access the data in these tables - path,task1Data,task2ClOrdsData,task2MktTradesData"
2024.04.22T02:07:01.334 start
2024.04.22T02:07:01.334 :C:\Users\sunny\Desktop\TestCode\kdb_tasks//tests/testCases.csv 18 test(s)
2024.04.22T02:07:01.362 end
"All tests passed... Saving results in ./tests/KUTR.csv"
```

In case of any test failure, you will see the below sample message and the Q process will get into DEBUG mode - 
```
q c:/Users/sunny/Desktop/TestCode/kdb_tasks/tests/run_tests.q -codePath "C:\Users\sunny\Desktop\TestCode\kdb_tasks"
KDB+ 4.0 2023.01.20 Copyright (C) 1993-2023 Kx Systems
w64/ 8(24)core 32538MB sunny sundew-zbook 192.168.1.114 EXPIRE 2024.07.13 sunnyshukla2008@gmail.com KOD #5015196

"Path has been specified as - C:\\Users\\sunny\\Desktop\\TestCode\\kdb_tasks"
"All code files loaded successfully"
"All data files loaded successfully"
"You can access the data in these tables - path,task1Data,task2ClOrdsData,task2MktTradesData"
2024.04.22T02:08:13.165 start
2024.04.22T02:08:13.165 :C:\Users\sunny\Desktop\TestCode\kdb_tasks//tests/testCases.csv 18 test(s)
2024.04.22T02:08:13.192 end
"Some tests have failed... Saving results in ./tests/KUTR.csv"
action ms bytes lang code                                                  repeat file                                                            msx bytesx ok okms okbytes valid timestamp  

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
true   1  50    q    all "773.8946"=string first .api.getTask2Vwap[5]`vwap 1      :C:\Users\sunny\Desktop\TestCode\kdb_tasks//tests/testCases.csv 0   0      0  1    1       1     2024.04.22T02:08:13.192
```

## Troubleshooting
In case the provided codePath is incorrect or for some reason, the process is unable to access the .q files in the given path, you may see the below error on startup - 

```
q c:/Users/sunny/Desktop/TestCode/kdb_tasks/core/init.q -codePath "C:\Users\sunny\Desktop\TestCode\kdb_tasks1"
KDB+ 4.0 2023.01.20 Copyright (C) 1993-2023 Kx Systems
w64/ 8(24)core 32538MB sunny sundew-zbook 192.168.1.114 EXPIRE 2024.07.13 sunnyshukla2008@gmail.com KOD #5015196

"Path has been specified as - C:\\Users\\sunny\\Desktop\\TestCode\\kdb_tasks1"
"Failure in loading a file via .init.q.... Exiting"
```