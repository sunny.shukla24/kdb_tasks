\d .mkt

// @kind function
// @param t         {dict}          this is the dictionary of trades
// @param syms      {symbol}        this is the RIC/FXPair for which we need to compute VWAP
// @param st        {timespan}      this is the startTime for which we need to compute VWAP
// @returns         {long}          returns the starting index 
getStartIdx:{[t;syms;st]
    (t[syms][`time]) binr st
 };

// @kind function
// @param t         {dict}          this is the dictionary of trades
// @param syms      {symbol}        this is the RIC/FXPair for which we need to compute VWAP
// @param et        {timespan}      this is the endTime for which we need to compute VWAP
// @param si        {long}          this is the starting index of the time range for which we need to compute VWAP
// @returns         {long}          returns the ending index 
getEndIdx:{[t;syms;et;si]
    si|(t[syms][`time]) bin et
 };

// @kind function
// @param si        {long}          this is the starting index of the time range
// @param ei        {long}          this is the ending index of the time range
// @returns         {long[]}        returns list of indices between a given pair of <start;end> indices
idxRange:{[si;ei]
    si+til each 0 | 1 + ei-si
 };

// @kind function
// @param syms      {symbol[]}      this is the list of RICs/FXPairs for which we need to compute VWAP
// @param st        {timespan[]}    this is the list of startTimes for which we need to compute VWAP
// @param et        {timespan[]}    this is the list of endTimes for which we need to compute VWAP
// @param s         {int[]}         this is the list of signs (1/-1) 
// @param lp        {float[]}       this is the list of limitPrices to be considered when computing VWAP
// @param ac        {symbol}        this is a symbol indicating whether we are computing VWAP for FXPairs or Equities <has possible values `fx/`equity>
// @returns         {float[]}       returns a vector of Volume Weighted Average Price
// @fileOverview This is a function that returns VWAP for provided vector of inputs
vwap:{[syms;st;et;s;lp;ac]
    $[ac=`fx;
        /We want VWAP for FX pairs - Task1
        t:.util.getFXTrades[enlist (in;`fxSym;enlist distinct syms)];
        $[ac=`equity;
            /We want VWAP for Equities data - Task2
            t:.util.getEquitiesTrades[enlist (in;`sym;enlist distinct syms)];
            '`$"Passed in assetClass param can only be one of <`fx or `equity>"
        ]
    ];

    /Creating projection to the base function
    f:vwapBasic[t;];

    /Actual run using peach (threads)
    .[f; ] peach flip (syms;st;et;s;lp)
 };

// @kind function
// @param t         {dict ()}       this is the dictionary of trades
// @param syms      {symbol}        this is the RIC/FXPair for which we need to compute VWAP
// @param st        {timespan}      this is the startTime for which we need to compute VWAP
// @param et        {timespan}      this is the endTime for which we need to compute VWAP
// @param s         {int}           this is the sign (1/-1) 
// @param lp        {float}         this is the limitPrice to be considered when computing VWAP
// @returns         {float}         returns a Volume Weighted Average Price
// @fileOverview This is a function that returns VWAP for provided inputs
vwapBasic:{[t;syms;st;et;s;lp]
    /Get starting index
    si:getStartIdx[t;syms;st];
    /Get ending index
    ei:getEndIdx[t;syms;et;si-1];
    /Get indices in the range
    indices:idxRange[si;ei];

    /Get all volumes
    v:t[syms][`size] indices;

    /Get all prices
    p:t[syms][`price] indices;

    if[0=count v;
        / No sizes/prices found
        :0n
     ];

    $[null lp;
        /No limitPx set or we are dealing with FXPairs
        [
            den:sum v;
            num:sum p*v
        ];
        [
            den:sum v where ?[s=1;p<=;p>=] lp;
            num:sum (p*v) where ?[s=1;p<=;p>=] lp
        ]
    ];

    /Return final vwap (null if no volume in the time range)
    num%?[0=den;0n;den]
 };

// @kind function
// @param syms      {symbol[]}      this is the list of RICs/FXPairs for which we need to compute TWAP
// @param st        {timespan[]}    this is the list of startTimes for which we need to compute TWAP
// @param et        {timespan[]}    this is the list of endTimes for which we need to compute TWAP
// @param ac        {symbol}        this is a symbol indicating whether we are computing VWAP for FXPairs or Equities <has possible values `fx/`equity>
// @returns         {float[]}       returns a vector of Time Weighted Average Price
// @fileOverview This is a function that returns TWAP for provided vector of inputs
twap:{[syms;st;et;ac]
    $[ac=`fx;
        /We want TWAP for FX pairs - Task1
        t:.util.getFXTrades[enlist (in;`fxSym;enlist distinct syms)];
        $[ac=`equity;
            /We want TWAP for Equities data - Task2
            t:.util.getEquitiesTrades[enlist (in;`rics;enlist distinct syms)];
            '`$"Passed in assetClass param can only be one of <`fx or `equity>"
        ]
    ];

    /Creating projection to the base function
    f:twapBasic[t;];

    /Actual run using peach (threads)
    .[f; ] peach flip (syms;st;et)
 };

// @kind function
// @param t         {dict ()}       this is the dictionary of trades
// @param syms      {symbol}        this is the RIC/FXPair for which we need to compute TWAP
// @param st        {timespan}      this is the startTime for which we need to compute TWAP
// @param et        {timespan}      this is the endTime for which we need to compute TWAP
// @returns         {float}         returns a Time Weighted Average Price
// @fileOverview This is a function that returns TWAP for provided inputs
twapBasic:{[t;syms;st;et]
    /Get starting index
    si:getStartIdx[t;syms;st];
    /Get ending index
    ei:getEndIdx[t;syms;et;si-1];
    /Get indices in the range
    indices:idxRange[si;ei];

    times:t[syms][`time] indices;
    prices:t[syms][`price] indices;

    $[0=count prices;
        /No trade found in the interval; return 0n
        0n;
        $[1=count prices;
            /Only one price, return that
            prices[0];
            /Return time-weighted price
            /Note we cannot return plain ((next times)-times) wavg prices, as we also need to consider the time from the start of the interval
            /till the first trade in the interval and the time from the last trade in the interval to the end of the window
            // @todo Some use cases may warrant adding an extra row at the start of the window using the prevailing price 
            // from the previous trade before the interval began, this depends on what is the user preference
            // to achieve this, we need to get times and prices from [si-1;ei] and then overwrite times[0] and prices[0]
            [
                times[0]:st;                        /Override the startTime with our interval till the next change
                w:(next times) - times;
                w[0]:max(w[0];0);
                cnt:count w;
                w[cnt-1]:max ((et-times[cnt-1]);0); /Override the endTime with our (interval end-last time)
                w wavg prices
            ]
        ]    
    ]
 };

// @kind function
// @param syms      {symbol[]}      this is the RIC (in vector form) for which we need to compute VWAP
// @param st        {time[]}        this is the list of startTimes when each version changes
// @param et        {time[]}        this is the list of endTimes for each interval
// @param s         {int[]}         this is the sign (in vector form) - Buy/Sell
// @param lp        {float[]}       this is the list of limitPrices to be considered when computing VWAP
// @returns         {float}         returns a re-stitched Volume Weighted Average Price
// @fileOverview This is a function that returns VWAP for an order where limitPrices may vary over order duration
calculateRestitchVWAP:{[syms;st;et;s;lp]
    /We want VWAP for Equities data - Task2
    t:.util.getEquitiesTrades[enlist (in;`sym;enlist distinct syms)];

    f:getVolAndPxInLimit[t;];
    res:.[f; ] peach flip (syms;st;et;s;lp);

    den:sum res[;1];   / Add all v
    num:sum res[;0];   / Add all (p*v)
    num%?[0=den;0n;den]
 };

// @kind function
// @param t         {dict ()}       this is the dictionary of trades
// @param sym       {symbol}        this is the RIC for which we need to compute VWAP
// @param st        {time}          this is the startTime for which we need to compute VWAP
// @param et        {time}          this is the endTime for which we need to compute VWAP
// @param s         {int}           this is the sign (buy/sell/shortSell) 
// @param lp        {float}         this is the limitPrice to be considered when computing VWAP
// @returns         {float}         returns a Volume Weighted Average Price
// @fileOverview This is a function that returns VWAP for provided inputs
getVolAndPxInLimit:{[t;sym;st;et;s;lp]
    /Get starting index
    si:getStartIdx[t;sym;st];
    /Get ending index
    ei:getEndIdx[t;sym;et;si-1];
    /Get indices in the range
    indices:idxRange[si;ei];

    /Get all volumes
    v:t[sym][`size] indices;

    /Get all prices
    p:t[sym][`price] indices;

    $[null lp;
        /No limitPx set
        [
            den:sum v;
            num:sum p*v
        ];
        [
            den:sum v where ?[s=1;p<=;p>=] lp;
            num:sum (p*v) where ?[s=1;p<=;p>=] lp
        ]
    ];

    /Return a list of (num;den)
    (num;den)
 };

\d .