path:first .Q.opt[.z.x][`codePath];
system ("l ",path,"/core/init.q");

\d .test

// @todo Move tests to qcumber

/Load Simon's K4Unit library
.init.loadLibrary[.init.code;"/tests/k4unit.q"];

/Load tests file
KUltf hsym `$.init.code,"/tests/testCases.csv";

/Run tests
KUrt[];

/Check results
$[(all KUTR`ok) & (all KUTR`okms) & (all KUTR`valid);
    / All tests have passed; display summary
    [
        show "All tests passed... Saving results in ./tests/KUTR.csv";
        show select action, msx, bytesx, code from KUTR;
        KUstr[];
        exit 0
    ];
    / Some tests have either failed OR/AND are invalid
    [
        show "Some tests have failed... Saving results in ./tests/KUTR.csv";
        show select from KUTR where ((not ok) or (not okms) or (not okbytes) or (not valid));
        exit 1
    ]
 ];