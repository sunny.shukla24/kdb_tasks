\d .api

// @kind function
// @param fxSymList {symbol[]}  this is the list of FXPairs for which we need to compute VWAP
// @param startTime {timespan}  this is the startTime of the time range during which we need to compute VWAP
// @param endTime   {timespan}  this is the endTime of the time range during which we need to compute VWAP
// @returns {table(fxSym:symbol[]; startTime:timespan[]; endTime:timespan[]; vwap:float[])}   returns Volume Weighted Average Price
// @fileOverview This is a function that returns VWAP for FX Pairs during a specified time range
getTask1Vwap:{[fxSymList;startTime;endTime]

    /Check if starTime & endTime are of the correct type
    if[not ((-16h=type startTime) & (-16h=type endTime));
        '`$"The inputs startTime and endTime should be of type - timespan"
      ];

    /Assuming start and endTimes are single times and not vectors, let's first create an input table
    t:update startTime:startTime, endTime:endTime from ([]fxSym:(),fxSymList);
    t:update vwap:.mkt.vwap[fxSym;startTime;endTime;(count i)#1;(count i)#0n;`fx] from t;
    
    /Return a table with limited columns
    select fxSym, startTime, endTime, vwap from t
 };

// @kind function
// @param fxSymList {symbol[]}  this is the list of FXPairs for which we need to compute TWAP
// @param startTime {timespan}  this is the startTime of the time range during which we need to compute TWAP
// @param endTime   {timespan}  this is the endTime of the time range during which we need to compute TWAP
// @returns {table(fxSym:symbol[]; startTime:timespan[]; endTime:timespan[]; twap:float[])}   returns Time Weighted Average Price
// @fileOverview This is a function that returns TWAP for FX Pairs during a specified time range
getTask1Twap:{[fxSymList;startTime;endTime]

    /Check if starTime & endTime are of the correct type
    if[not ((-16h=type startTime) & (-16h=type endTime));
        '`$"The inputs startTime and endTime should be of type - timespan"
      ];
      
    /Assuming start and endTimes are single times and not vectors, let's first create an input table
    t:update startTime:startTime, endTime:endTime from ([]fxSym:(),fxSymList);
    t:update twap:.mkt.twap[fxSym;startTime;endTime;`fx] from t;
    
    /Return a table with limited columns
    select fxSym, startTime, endTime, twap from t
 };

// @kind function
// @param orderId {long}  this is the client orderId for which we need to compute VWAP
// @returns {table(id:long[]; sym:symbol[]; startTime:timespan[]; endTime:timespan[]; vwap:float[])}   returns Volume Weighted Average Price
// @fileOverview This is a function that returns VWAP for client orders keeping in mind the limitPx conditions
getTask2Vwap:{[orderId]
  /Fetch details for this orderId from task2ClOrdsData table
  t:select from `.[`task2ClOrdsData] where id=orderId;

  /What if orderId doesn't exist?
  if[0=count t;
    '`$"Incorrect orderId entered. Please check and try again... the possible orderIds are in task2ClOrdsData"
   ];

  /If we have a plain vanilla case of no change in limitPx during the order duration OR no limitPx set at all OR only 1 version
  /Use the function we wrote in Task1
  if[(1=count t) or (1=count distinct t`limit);
    :select sym, side, startTime, endTime, vwap from 
        update vwap:.mkt.vwap[sym;startTime;endTime;?[side=`B;1;-1];limit;`equity] from 0!select by orderId from t
  ];

  /As each order version may have a different limitPx, we need to essentially get (sum size*price) & (sum size) for each version 
  /We can then calculate vwap by restitching the data
  t:update endTime:endTime^(next time) by id from t;
  :select sym, side, startTime, endTime, vwap from
        update vwap:enlist .mkt.calculateRestitchVWAP[t`sym;t`time;t`endTime;?[t`side=`B;1;-1];t`limit] from select by orderId from t
 };

\d .