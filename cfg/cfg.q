\d .cfg

numOfMktDataRows:100000;
numOfOrders:5;
numOfOrderAuditTrail:20;

/****************** TASK 1 ******************/
task1FXPairs:`EURUSD`GBPINR`USDGBP;
task1EODFX:(task1FXPairs!(1.06;103.12;0.81));
task1FileName:"task1.csv";

/****************** TASK 2 ******************/
task2RICs:`VOD.L`ISF.L`VOLVb.ST;
task2EODPx:(task2RICs!(67.00;772.80;280.20));
task2LimitPx:(task2RICs!(68.00;773.00;278.60));
task2ClOrdsFileName:"task2clOrds.csv";
task2MktDataFileName:"task2MktData.csv";

\d .